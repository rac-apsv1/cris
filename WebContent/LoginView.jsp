<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
</head>
<body>
	<c:if test="${not (empty user)}">
		<p>You are authenticated as ${user.id}</p>
		<p><a href="LogoutServlet">Logout</a></p>
	</c:if>
	<c:if test="${empty user}">
		<a href="LoginServlet">Login</a>
	</c:if>
	<p style="color: red;">${message}</p>
	<h3><a href="ResearchersListServlet">Researchers list</a></h3>

	<form action="LoginServlet" method="post">
		<input type="text" name="email" placeholder="Email"> <input
			type="password" name="password" placeholder="Password">
		<button type="submit">Login</button>
	</form>
	
	
	<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="authors" placeholder="Authors">
        <input type="date" name="Publicationdate" placeholder="Publication Date">
        <input type="text" name="Publicationname" placeholder="Publication Name">
        <button type="submit">Create publication</button>
</form>


</body>
</html>