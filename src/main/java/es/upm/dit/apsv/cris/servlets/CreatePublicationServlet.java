package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;



@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = (String) request.getParameter("id");
		String title = (String) request.getParameter("title");
		String authors = (String) request.getParameter("authors");
		String publicationdate = (String) request.getParameter("Publicationdate");
		String publicationname = (String) request.getParameter("Publicationname");
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Researcher user = null;
		
		//Check that we are not root, and we are an actual researcher
		try {
			user = (Researcher) request.getSession().getAttribute("user");
			user = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" + user.getId())
					.request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
		} catch (Exception e) {}
		
		if ((user == null) || (user.getId().equals("root"))) {
			//Set message
			request.setAttribute("message", "Only a researcher can add publications to itself");
			//Finally forwarding to the Researcher View
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
			return;
		}
		
		//Check if the id does not exist yet
		try {
			//Receive the JSON and convert to Publication object
			Publication publication = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publication/" + id)
					.request().accept(MediaType.APPLICATION_JSON).get(Publication.class);
			if (publication != null) {
				//Set message
				request.setAttribute("message", "Publication Id already exists");
				//Finally forwarding to the Researcher View
				getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request, response);
				return;
			}
		} catch (Exception e){}
		
		
		//Create new publication object from the params
		Publication publication_new = new Publication();
		publication_new.setId(id);
		publication_new.setTitle(title);
		publication_new.setAuthors(authors);
		publication_new.setPublicationDate(publicationdate);
		publication_new.setPublicationName(publicationname);
		
		try {
			//We start a client and send the publication to create to the rest service
			client.register(JsonProcessingFeature.class)
					.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/").request()
					.post(Entity.entity(publication_new, MediaType.APPLICATION_JSON), Response.class);
		} catch (Exception e) {}
		
		//Finally redirecting to the Researcher Servlet
		response.sendRedirect(request.getContextPath() + "/ResearcherServlet?id=" + user.getId());

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
